---
lang: "es"
title: ""
subtitle: ""
author: "Francisco Galindo"
date: ""
numbersections: true
autoEqnLabels: true
# toc: true
# bibliography: Biblio.bib
# abstract: ""

papersize: a4
documentclass: article
geometry:
- top=30mm
- left=20mm
- right=20mm
- bottom=30mm

header-includes: |
 \usepackage{float}
 \let\origfigure\figure
 \let\endorigfigure\endfigure
 \renewenvironment{figure}[1][2] {
   \expandafter\origfigure\expandafter[H]
 } {
   \endorigfigure
 }

# figureTitle: "Figura"
# tableTitle: "Tabla"
# figPrefix: "fig."
# eqnPrefix: "ec."
# tblPrefix: "tbl."
# loftitle: "# Lista de figuras"
# lotTitle: "# Lista de tablas"
---
