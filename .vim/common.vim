filetype plugin indent on
syntax on

set encoding=utf-8
set fileformat=unix
set path+=**
" set hlsearch
set incsearch
set ignorecase
set smartcase
set smartindent
set number relativenumber
set signcolumn=yes
set hidden
" set clipboard+=unnamedplus
set colorcolumn=80
set wildmode=longest,list,full
set wildignorecase
set updatetime=500
set ttimeoutlen=100
set mouse=a

" Removes trailing whitespace when saving a file
" autocmd BufWritePre * %s/\s\+$//e

" ctrl+c closes the current buffer, but keeps split
" nnoremap <C-c> :bp\|bd #<CR>

set termguicolors

" Functions
function! ConvertToTabs(width)
	execute "setlocal ts=" . a:width . " noexpandtab"
	%retab!
	setlocal ts=8
endfunction

function! ConvertToSpaces(width)
	execute "setlocal ts=" . a:width " sw=" . a:width " expandtab"
	%retab!
endfunction
