#If not running interactively, don't do anything
[[ $- != *i* ]] && return

# zmodload zsh/zprof

source ~/.zsh/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

export EDITOR='nvim'
export PATH=$HOME/.local/bin:$PATH

setopt autocd
setopt completealiases
setopt PROMPT_SUBST

autoload -U colors && colors

autoload -U compinit && compinit -u
zstyle ':completion:*' menu select

# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' \
	'm:{a-z\-}={A-Z\_}' \
	'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
	'r:|?=** m:{a-z\-}={A-Z\_}'


function showPath()
{
	echo $(dirs | perl -F/ -ane 'print join( "/", map { $i++ < @F - 1 ?  substr $_,0,1 : $_ }@F)')
}

# Find and set branch name var if in git repository.
function git_branch_name()
{
	branch=$(git symbolic-ref HEAD 2> /dev/null | awk 'BEGIN{FS="/"} {print $NF}')
	if [[ $branch == "" ]]; then
		:
	else
		echo ' ('$branch')'
	fi
}

PROMPT='%F{green}%n%f@%m%f:%F{green}$(showPath)%f$(git_branch_name) $ '
RPROMPT='[%F{yellow}%?%f]'

HISTSIZE=10000
SAVEHIST=10000
HISTDUP=erase
HISTFILE=~/.cache/zsh/history
setopt    appendhistory     # Append history to the history file (no overwriting
setopt    sharehistory      # Share history across terminals
setopt    incappendhistory  # Immediately append to the history file, not just when a term is killed
setopt    hist_ignore_space  # Avoid commands from getting to the history is prepended with a space

# vi mode
bindkey -v
bindkey "^?" backward-delete-char
bindkey "^W" backward-kill-word
bindkey "^H" backward-delete-char
bindkey "^U" backward-kill-line

export KEYTIMEOUT=1

# Alias
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias cp='cp -ip'
alias mv='mv -i'
alias rm='rm -I'
alias virtual-loopback="sudo modprobe v4l2loopback video_nr=3,4 card_label='V4L2 Virtual Camera','V4L2 Window Streaming' exclusive_caps=1"
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias vim='nvim'

alias dockerBd1='docker start c1-bd-fgm && docker attach c1-bd-fgm'
alias dockerBd1T='docker exec -it c1-bd-fgm bash'

# zprof
