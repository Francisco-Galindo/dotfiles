#!/usr/bin/env bash

# A script to perform incremental backups using rsync

set -o errexit
set -o nounset
set -o pipefail

readonly SOURCE_DIR="${HOME}"
# readonly BACKUP_DIR="/mnt/backup"
readonly BACKUP_DIR="${HOME}/mount"
readonly DATETIME="$(date '+%F')"
readonly BACKUP_PATH="${BACKUP_DIR}/${DATETIME}"
readonly LATEST_LINK="${BACKUP_DIR}/latest"
readonly LOG_DIR="${HOME}/.backup-dir"

mkdir -p "${BACKUP_DIR}"
mkdir -p "${LOG_DIR}"

rsync -av --delete \
  "${SOURCE_DIR}/" \
  --link-dest "${LATEST_LINK}" \
  --exclude=".cache" \
  --exclude=".local/share/containers/" \
  --exclude=".local/share/flatpak/" \
  --exclude="mount/" \
  --info=progress2 \
  "${BACKUP_PATH}" 2> "${LOG_DIR}/backup-${DATETIME}.err"

rm -rf "${LATEST_LINK}"
ln -s "${BACKUP_PATH}" "${LATEST_LINK}"
