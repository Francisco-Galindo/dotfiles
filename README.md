# Dotiles

## Deplying the dotfiles

Run these commands on a (preferably) fresh minimalist install of your chosen OS:

```sh
$ alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
$ git clone --bare https://gitlab.com/Francisco-Galindo/dotfiles.git $HOME/.dotfiles
$ dotfiles checkout
```

If you get an error like this after running the last command:

```
error: The following untracked working tree files would be overwritten by checkout:
    .gitignore
    * other files *
Please move or remove them before you can switch branches.
Aborting
```

Then, move or delete the files and rerun `dotfiles checkout`

### Vim plugins

Install `node` and `go` in order to make the vim plugins work

Also, install `vim plug`:

```sh
$ curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

### Zsh plugins

```sh
$ mkdir ~/.zsh
$ git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions\
$ git clone https://github.com/zdharma-continuum/fast-syntax-highlighting ~/.zsh/fast-syntax-highlighting
```

### GTK themes and icons

Install `sassc` `xz` and `inkscape` in order to build the themes

Download, build and install the themes:

```sh
$ curl -Lo ~/mint-themes_2.1.5.tar.xz http://packages.linuxmint.com/pool/main/m/mint-themes/mint-themes_2.1.5.tar.xz
$ tar -xf ~/mint-themes_2.1.5.tar.xz
$ cd mint-themes/
$ make
$ mkdir -p ~/.local/share/themes/
$ mv usr/share/themes/Mint-Y usr/share/themes/Mint-Y-Dark ~/.local/share/themes
$ cd ~ && rm -r ~/mint-themes_2.1.5.tar.xz ~/mint-themes/
```

Download and install the icons:

```sh
$ curl -Lo ~/mint-y-icons_1.6.7.tar.xz http://packages.linuxmint.com/pool/main/m/mint-y-icons/mint-y-icons_1.6.7.tar.xz
$ tar -xf ~/mint-y-icons_1.6.7.tar.xz
$ mkdir -p ~/.local/share/icons/
$ mv mint-y-icons/usr/share/icons/Mint-Y ~/.local/share/icons/
$ rm -r mint-y-icons/ mint-y-icons_1.6.7.tar.xz
```
