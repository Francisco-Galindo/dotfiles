setlocal textwidth=80

nmap <buffer><silent> <leader>p :call mdip#MarkdownClipboardImage()<CR>

nnoremap ,doc :-1read /home/paco/.vim/snippets/pandoc-doc.md<CR>2jf"a
nnoremap ,pres :-1read /home/paco/.vim/snippets/pandoc-pres.md<CR>jf"a
