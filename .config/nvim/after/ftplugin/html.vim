setlocal tabstop=2
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal expandtab

" Snippets
nnoremap ,skel :-1read $HOME/.vim/snippets/html-template.html<CR>5jf>a
