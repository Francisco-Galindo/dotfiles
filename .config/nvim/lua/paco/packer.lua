-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- commit '5a01247' breaks srcery function name colors
  -- use {'srcery-colors/srcery-vim', as = 'srcery', commit = 'cb1647d'}
  use {'srcery-colors/srcery-vim', as = 'srcery'}
  use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
  use {'nvim-treesitter/playground'}
  use {'tpope/vim-fugitive'}
  use {'mhinz/vim-signify'}
  use {'mattn/emmet-vim'}
  use {'img-paste-devs/img-paste.vim'}

  use{'Francisco-Galindo/flexoki-neovim', as = 'flexoki'}

  use {'lervag/vimtex'}

  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  use {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  }

  use {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v2.x',
    requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},             -- Required
      {                                      -- Optional
        'williamboman/mason.nvim',
        run = function()
          pcall(vim.cmd, 'MasonUpdate')
        end,
      },
      {'williamboman/mason-lspconfig.nvim'}, -- Optional

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},     -- Required
      {'hrsh7th/cmp-nvim-lsp'}, -- Required
      {'L3MON4D3/LuaSnip'},     -- Required
    }
  }

end)
