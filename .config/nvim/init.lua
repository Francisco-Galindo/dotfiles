require("paco")

vim.cmd('source ~/.vim/common.vim')
vim.cmd('set spelllang=es,en')
vim.cmd('set updatetime=300')
vim.cmd('colorscheme flexoki-dark')
vim.cmd('set cursorline!')
