# -*- coding: utf-8 -*-

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
# terminal = guess_terminal()
terminal = "kitty -e fish"

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    #Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    #Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "j", lazy.layout.next(),
        desc="Move window focus to other next"),
    Key([mod], "k", lazy.layout.previous(),
        desc="Move window focus to previous window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "Tab", lazy.prev_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # Custom keybindings
    Key([mod], "space", lazy.widget["keyboardlayout"].next_keyboard(),
        desc="Next keyboard layout"),
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    Key([mod, "shift"], "s", lazy.spawn("flameshot gui")),
    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run")),
    Key([mod], "b", lazy.spawn("firefox")),
    Key([mod], "c", lazy.spawn("qalculate-gtk")),

    # Swapping columns
    #Key([mod, "shift", "control"], "h", lazy.layout.swap_column_left()),
    #Key([mod, "shift", "control"], "l", lazy.layout.swap_column_right()),

]

group_names = "         ".split()

groups = [Group(name) for name in group_names]

for i, (name) in enumerate(group_names, 1):
    i = str(i)[-1]
    keys.append(Key([mod], i, lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], i, lazy.window.togroup(name))) # Send current window to another group

layout_theme = {
    "border_width": 3,
    "margin": 15,
    "border_focus": "#99ff30"
}

layouts = [
    #layout.Columns(border_focus_stack='#d75f5f', margin=4),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

tokyo_colors = {
    "bar_bkgnd": ["#11121d", "#11121d"], # 0, bar background
    "grpbox_bkgnd": ["#11121d", "#11121d"], # 1, group box background
    "hl_bkgnd": ["#9a7ecc", "#9a7ecc"], # 2, highlight background
    "grp_w_wind_font": ["#f1efee", "#f1efee"], # 3, font for group with windows open
    "otr_indic": ["#7eae4a", "#7eae4a"], # 5, border line color for groups on other monitors
    "cur_indic": ["#b9f27c", "#b9f27c"], # 7, border line color for current group
    "grp_inac_font": ["#a9b1d6", "#a9b1d6"]} # 8, font for inactive groups

minino_colors = {
    "bar_bkgnd": ["#1b1918", "#1b1918"], # 0, bar background
    "grpbox_bkgnd": ["#1b1918", "#1b1918"], # 1, group box background
    "hl_bkgnd": ["#766e6b", "#766e6b"], # 2, highlight background
    "grp_w_wind_font": ["#f1efee", "#f1efee"], # 3, font for group with windows open
    "otr_indic": ["#00511b", "#00511b"], # 5, border line color for every other tab
    "cur_indic": ["#509e33", "#509e33"], # 7, window name
    "grp_inac_font": ["#a8a19f", "#a8a19f"]} # 8, backbround for inactive screens

colors = minino_colors
# colors = tokyo_colors

widget_defaults = dict(
    font='Ubuntu Mono',
    fontsize=12,
    padding=2,
    background=colors["bar_bkgnd"],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(),
                widget.GroupBox(
                    font = "Ubuntu Bold",
                    fontsize = 15,
                    active = colors["grp_w_wind_font"],
                    inactive = colors["grp_inac_font"],
                    rounded = False,
                    highlight_color = colors["hl_bkgnd"],
                    highlight_method = "line",
                    this_current_screen_border = colors["cur_indic"], # Current group on this screen
                    this_screen_border = colors["otr_indic"],
                    other_current_screen_border = colors["cur_indic"],
                    other_screen_border = colors["otr_indic"],
                    foreground = colors["grp_w_wind_font"],
                    background = colors["grpbox_bkgnd"]
                    ),
                widget.Prompt(),
                widget.TextBox(text="  Window:",
                    font = "DejaVu Sans Mono"),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(),
                widget.Net(),
                widget.KeyboardLayout(configured_keyboards=['us','es']),
                widget.Clock(format='|  %H:%M %p|  %a %Y-%m-%d |'),
                widget.QuickExit(font = "Ubuntu Bold",
                    fontsize = 14,
                    default_text=""
                    ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='pinentry-gtk-2'),
    Match(title='Qalculate!'),
    Match(title='Nitrogen'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
